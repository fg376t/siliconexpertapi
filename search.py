# Select Component
from __future__ import print_function, unicode_literals
from PyInquirer import prompt, print_json
#SE
import urllib.parse
import urllib.request  
import http.cookiejar
# XML to JSON
from xmljson import parker as bf
from xml.etree.ElementTree import fromstring
import json
import jsbeautifier
# CSV
import csv
import os
# file checking
import sys
import os
import csv
import re
import subprocess

partNumber="GRM188R60J476ME15"
#partNumber="CRCW040210K0FK"
#partNumber="NCP3235MNTXG"

############ SE query ############
os.system('cls')
print('\n================ Querying SiliconExpert Keyword Search API for MFRPN = ' + partNumber + ' ================\n')

cookies=http.cookiejar.CookieJar()
handlers = [
    urllib.request.HTTPHandler(),
    urllib.request.HTTPSHandler(),
    urllib.request.HTTPCookieProcessor(cookies)
]
userName="on_semi_demo"
password="E!b52Al,%24kq" # %24 is an escaped $, actual password is E!b52Al,$kq

fmt="json" # this is being ignored, converted manually from xml to json using json.dumps
# https://app.siliconexpert.com/ProductAPI/search/authenticateUser?login=on_semi_demo&apiKey=E!b52Al,%24kq
url = 'https://app.siliconexpert.com/ProductAPI/search/authenticateUser?login='+userName+'&apiKey='+password
req = urllib.request.Request(url)
opener = urllib.request.build_opener(*handlers)
o=opener.open(req)

############ Keyword Search ############
# https://app.siliconexpert.com/ProductAPI/search/partsearch?partNumber=GRM155R61C105KA12
searchUrl='https://app.siliconexpert.com/ProductAPI/search/partsearch?'+'fmt='+fmt+'&partNumber='+partNumber
req = urllib.request.Request(searchUrl)
opener = urllib.request.build_opener(*handlers)
o=opener.open(req)
query_xml=o.read().decode('utf-8')
#print(query_xml)

############ XML to JSON ############
# For some reason the SE returns XML even though the default is JSON
keyword_json = bf.data(fromstring(query_xml))
#print(json.dumps(keyword_json, ensure_ascii=False))

############ Select Component ############
MFR_MFRPN = []
if isinstance(keyword_json['Result'], list):	
	# Result is an array
	for result in keyword_json['Result']:
		MFR_MFRPN.append(result['Manufacturer'] + ' ' + result['PartNumber'])
else:
	# Result is an object
	MFR_MFRPN.append(keyword_json['Result']['Manufacturer'] + ' ' + keyword_json['Result']['PartNumber'])

results_found = len(MFR_MFRPN)
print('Results found = ' + str(results_found))

questions = [
    {
        'type': 'list',
        'name': 'component',
        'message': 'Select the desired component',
		'choices': MFR_MFRPN
    }
]
answers = prompt(questions)

############ Part Detail Search ############
if results_found > 1:
	component_index = MFR_MFRPN.index(answers["component"])
	ComID = json.dumps(keyword_json['Result'][component_index]['ComID'], ensure_ascii=False)
else:
	ComID = json.dumps(keyword_json['Result']['ComID'], ensure_ascii=False)
	
os.system('cls')
print('\n================ Querying SiliconExpert Part Detail Search API for ComID = ' + ComID + ' ================\n')

# https://app.siliconexpert.com/ProductAPI/search/partsearch?partNumber=GRM155R61C105KA12
searchUrl='https://app.siliconexpert.com/ProductAPI/search/partDetail?'+'fmt='+fmt+'&comIds='+ComID
req = urllib.request.Request(searchUrl)
#req.add_header('Accept', 'text/json') # TRY THIS IN THE FUTURE TO GET JSON, WOULD NOT NEED TO CONVERT
opener = urllib.request.build_opener(*handlers)
o=opener.open(req)
part_detail_xml=o.read().decode('utf-8')
# print(part_detail_xml)

############ XML to JSON ############
# For some reason the SE returns XML even though the default is JSON
part_detail_json = bf.data(fromstring(part_detail_xml))
opts = jsbeautifier.default_options()
opts.indent_size = 2
print(jsbeautifier.beautify(json.dumps(part_detail_json, ensure_ascii=False), opts))

#print(json.dumps(part_detail_json, ensure_ascii=False))
input('\nThe following information was found. Press Enter to continue.')
# input()


#print(json.dumps(answers["component"], ensure_ascii=False))  # use the answers as input for your app
Automotive = json.dumps(part_detail_json['Results']['ResultDto']['SummaryData']['AECQualified'], ensure_ascii=False)
if results_found > 1:
	component_index = MFR_MFRPN.index(answers["component"])
	TaxonomyPath = json.dumps(keyword_json['Result'][component_index]['TaxonomyPath'], ensure_ascii=False)
	Part_Type = TaxonomyPath.replace(' > ', '\\')
	ComID = json.dumps(keyword_json['Result'][component_index]['ComID'], ensure_ascii=False)
	Description = json.dumps(keyword_json['Result'][component_index]['Description'], ensure_ascii=False)
	MFR = json.dumps(keyword_json['Result'][component_index]['Manufacturer'], ensure_ascii=False)
	MFRPN = json.dumps(keyword_json['Result'][component_index]['PartNumber'], ensure_ascii=False)
	Datasheet = json.dumps(keyword_json['Result'][component_index]['Datasheet'], ensure_ascii=False)
else:
	TaxonomyPath = json.dumps(keyword_json['Result']['TaxonomyPath'], ensure_ascii=False)
	Part_Type = TaxonomyPath.replace(' > ', '\\')
	ComID = json.dumps(keyword_json['Result']['ComID'], ensure_ascii=False)
	Description = json.dumps(keyword_json['Result']['Description'], ensure_ascii=False)
	MFR = json.dumps(keyword_json['Result']['Manufacturer'], ensure_ascii=False)
	MFRPN = json.dumps(keyword_json['Result']['PartNumber'], ensure_ascii=False)
	Datasheet = json.dumps(keyword_json['Result']['Datasheet'], ensure_ascii=False)



### SPLIT ###


try:
	for Feature in part_detail_json['Results']['ResultDto']['ParametricData']['Features']:
		if 'Value' in Feature['FeatureName']:
			Value = json.dumps(Feature['FeatureValue'], ensure_ascii=False)
			pass
			break
	try:
		Value
	except NameError:
		raise Exception()
except:
	Value = MFRPN
	print('Value not found, using MFRPN')

try:
	for Feature in part_detail_json['Results']['ResultDto']['ParametricData']['Features']:
		if 'Tolerance' in Feature['FeatureName']:
			Tolerance = json.dumps(Feature['FeatureValue'], ensure_ascii=False)
			pass
			break
	try:
		Tolerance
	except NameError:
		raise Exception()
except:
	Tolerance = ''
	print('Tolerance not found')

try:
	for Feature in part_detail_json['Results']['ResultDto']['ParametricData']['Features']:
		if 'Voltage' in Feature['FeatureName']:
			Voltage = json.dumps(Feature['FeatureValue'], ensure_ascii=False)
			pass
			break
	try:
		Voltage
	except NameError:
		raise Exception()
except:
	Voltage = ''
	print('Voltage not found')
	
try:
	for Feature in part_detail_json['Results']['ResultDto']['PackageData']['Feature']: # Feature not Features here
		if 'Package/Case' in Feature['FeatureName']:
			Package = json.dumps(Feature['FeatureValue'], ensure_ascii=False)
			pass
			break
	try:
		Package
	except NameError:
		raise Exception()
except:
	try:
		Package = json.dumps(str(part_detail_json['Results']['ResultDto']['PackageData']['SupplierPackage']), ensure_ascii=False)
	except:
		Package = ''
		print('Package not found')

try:
	for Feature in part_detail_json['Results']['ResultDto']['PackageData']['Feature']: # Feature not Features here
		if 'Product Height' in Feature['FeatureName']:
			Height = json.dumps(Feature['FeatureValue'], ensure_ascii=False)
			pass
			break
	try:
		Height
	except NameError:
		raise Exception()
except:
	Height = ''
	print('Height not found')


print('\n')

####### SPLIT #####


# Check if CDS_SITE env variable exists and create paths to be used for this script
if "CDS_SITE" in os.environ:
	pcb_footprints_path = os.path.join(os.getenv("CDS_SITE"),"PCB_Footprints")
	pcb_padstacks_path = os.path.join(os.getenv("CDS_SITE"),"PCB_Padstacks")
	logic_path = os.path.join(os.getenv("CDS_SITE"),"Logic")
	database_path = os.path.join(os.getenv("CDS_SITE"),"Database")
else:
	print("CDS_SITE environmental variable is not configured correctly. See Cadence installation tutorial. Press any key to exit.")
	input()
	exit()

# Get all OLB file names
logic_files = []
for logic_file in os.listdir(logic_path):
    if logic_file.endswith(".OLB") or logic_file.endswith(".olb"):
        logic_files.append(os.path.splitext(logic_file)[0])
logic_files = list(map(str.lower, logic_files))
#print(logic_files)

subprocess.Popen(r'explorer "%s"' % logic_path)
questions = [
	{
		'type': 'input',
		'name': 'olb',
		'message': 'Enter the name of the schematic OLB file (without the OLB extension):',
	}
]
while 1:
	os.system('cls')
	print('\n================ Configure OLB Schematic Library ================\n')
	answer = prompt(questions)['olb']
	if not answer:
		print('Enter a valid OLB file name that exists in %s:' % logic_path)
		continue
	print(answer)
	for logic_file in logic_files:
		if answer in logic_file:
			Schematic_Part = answer.upper()
			break
	else:
		print('Enter a valid OLB file name that exists in %s:' % logic_path)
		continue
	break

####### SPLIT #####

#get PCBLE footprint names
pcble_fpx_footprints = []
with open(os.path.join(database_path, 'ussec.fpx')) as pcble_fpx:
	pcble_fpx_reader = csv.reader(pcble_fpx, delimiter='\t')
	next(pcble_fpx_reader) # skip header row
	for row in pcble_fpx_reader:
		# row 4 = Footprint Name
		pcble_fpx_footprints.append(row[4].upper())
# print(pcble_fpx_footprints)
# input()

#check if dra exists in library

# Get all DRA file names
# need to check in future for *sm file as well
pcb_footprint_files = []
pcb_footprint_files_ext = []
for pcb_footprint_file in os.listdir(pcb_footprints_path):
	if pcb_footprint_file.endswith(".dra") or pcb_footprint_file.endswith(".DRA"):
		pcb_footprint_files.append(os.path.splitext(pcb_footprint_file)[0])
		pcb_footprint_files_ext.append(pcb_footprint_file)
pcb_footprint_files = list(map(str.upper, pcb_footprint_files))
pcb_footprint_files_ext = list(map(str.upper, pcb_footprint_files_ext))
# print(pcb_footprint_files)
# print(pcb_footprint_files_ext)
# input()

#get PCB_Footprint name

questions = [
	{
		'type': 'input',
		'name': 'dra',
		'message': 'Enter a DRA name that exists in ' + os.path.join(database_path, 'ussec.fpx') + ':',
	}
]

while 1:
	os.system('cls')
	print('\n================ Configure DRA Footprint Library ================\n')
	answer = prompt(questions)['dra'].upper()
	if not answer:
		print('Enter a DRA name that exists in %s:' % os.path.join(database_path, 'ussec.fpx'))
		continue
	#check if it exists in FPX file
	if answer in pcble_fpx_footprints:
		PCB_Footprint = answer
		break
	else:
		print('Enter a DRA name that exists in %s:' % os.path.join(database_path, 'ussec.fpx'))
		continue
	#check if dra exists in library ##### HEEEEERRRRREEEE ######
	if answer in pcble_fpx_footprints:
		break
	else:
		print('Enter a DRA name that exists in %s:' % pcb_footprints_path)
		continue
	break


####### SPLIT #####

os.system('cls')
print('\n================ Confirm Component Properties ================\n')

Part_Type = Part_Type.replace('"', "")
Description = Description.replace('"', "")
MFR = MFR.replace('"', "")
MFRPN = MFRPN.replace('"', "")
Value = Value.replace('"', "")
Package = Package.replace('"', "")
Voltage = Voltage.replace('"', "")
Tolerance = Tolerance.replace('"', "")
Silkscreen = ''
Height = Height.replace('"', "")
Datasheet = Datasheet.replace('"', "")
Automotive = Automotive.replace('"', "")
CLASS = ''

print('Part Type = ' + Part_Type)
print('Description = ' + Description)
print('MFR = ' + MFR)
print('MFRPN = ' + MFRPN)
print('Value = ' + Value)
print('Package = ' + Package)
print('Voltage = ' + Voltage)
print('Tolerance = ' + Tolerance)
print('Silkscreen = ' + Silkscreen) # need user input still
print('Height = ' + Height)
print('Schematic Part = ' + Schematic_Part)
print('PCB Footprint = ' + PCB_Footprint) 
print('Datasheet = ' + Datasheet)
print('Automotive = ' + Automotive)
print('CLASS = ' + '') # need user input still

input('\nPress Enter to continue.')

####### SPLIT #####

# could have step here that gets all taxonomy to create necessary CSV files
# https://app.siliconexpert.com/ProductAPI/search/parametric/getAllTaxonomy

####### SPLIT #####

os.system('cls')
print('\n================ Writing to database CSV file ================\n')

CDS_SITE = os.environ['CDS_SITE']
category = Part_Type.partition('\\')[0]
category_csv = os.path.join(CDS_SITE, 'Database')
category_csv = os.path.join(category_csv, category+'.csv')
print('Writing to database CSV file: %s' % category_csv);


if not os.path.isfile(category_csv):
	with open(category_csv, 'w', newline='') as csvfile:
		filewriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
		filewriter.writerow(['Part Type','Description','MFR','MFRPN','Value','Package','Voltage','Tolerance','Silkscreen','Height','Schematic Part','PCB Footprint','Datasheet','Automotive','CLASS'])

with open(category_csv, 'a', newline='') as csvfile:
	filewriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
	filewriter.writerow([Part_Type, Description, MFR, MFRPN, Value, Package, Voltage, Tolerance, Silkscreen, Height, Schematic_Part, PCB_Footprint, Datasheet, Automotive, CLASS])

input('\nPress Enter to continue.')







